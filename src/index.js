const a = "hello";

console.log(`${a}, world!`);

const timeClock = document.getElementById("timeClock");

const parsetwoDigit = (time) => {
  return `00${time}`.slice(-2);
};

const showClock = () => {
  const now = new Date();
  const msg = `現在時刻は、\
    ${parsetwoDigit(now.getHours())}: \
    ${parsetwoDigit(now.getMinutes())}: \
    ${parsetwoDigit(now.getSeconds())}`;
  timeClock.innerHTML = msg;
};
setInterval(showClock, 1000);
